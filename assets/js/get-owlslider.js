$(document).ready(function(){ 
    $('#owl-app').owlCarousel({
        loop:true,
        nav:true,
        navText: ['<span class="span-roundcircle left-roundcircle"><i class="material-icons left-arrow-icon">play_arrow</i></span>','<span class="span-roundcircle right-roundcircle"><i class="material-icons right-arrow-icon">play_arrow</i></span>'],
        dots: false,
        stagePadding: 0,
        margin:0,
        smartSpeed:2000,
        responsive:{
            0:{
                items:1 
            },
            600:{
                items:1
            },
            1000:{
                items:1
            },
            1180:{
              items:1
            } 
        }
      });
      
    $('#client-owl').owlCarousel({
        loop:false,
        // navText: ['<span class="span-roundcircle left-roundcircle"><i class="fe fe-arrow-left left-arrow"></span>','<span class="span-roundcircle right-roundcircle"><i class="fe fe-arrow-right right-arrow"></span>'],
        dots: false,
        nav: false,
        stagePadding: 0,
        margin:10,
        autoplay:true,
        smartSpeed:2000,
        responsive:{
            0:{
                nav: false,
                items:3,
            },
            600:{
                nav: false,
                items:3
            },
            1200:{
                nav: false,
                items:3
            }
       }
       
    });
});

